from __future__ import unicode_literals

from django.db import models
import datetime

# Create your models here.

class Location(models.Model):
	lng                    = models.CharField(max_length=255)
	lat                    = models.CharField(max_length=255)

# class Shop(models.Model):
# 	domain                    = models.CharField(max_length=255, unique=True, db_column='shop_url')
# 	name                      = models.CharField(max_length=255, db_column='shop_name')
# 	shop_email                = models.CharField(max_length=255)
# 	access_token              = models.CharField(max_length=255)
# 	login_cookie              = models.CharField(max_length=255)
# 	currency                  = models.CharField(max_length=255, default='USD')
# 	is_installed              = models.BooleanField(default=True)
# 	created_on                = models.DateTimeField(auto_now_add=True)
# 	updated_on                = models.DateTimeField(auto_now_add=True)
# 	recurring_charge_id       = models.IntegerField(null=True, blank=True)

# class BarSettings(models.Model):
# 	shop                      = models.ForeignKey('Shop')
# 	bar_text 				  = models.TextField(default="")
# 	bar_color 				  = models.TextField(default="black")
# 	bar_enable 				  = models.BooleanField(default=True)
# 	timer_enable 			  = models.BooleanField(default=False)
# 	timer_countdown 		  = models.IntegerField(default=0)
# 	created_on                = models.DateTimeField(auto_now_add=True)
# 	updated_on                = models.DateTimeField(auto_now_add=True)

# class CartBarSettings(models.Model):
# 	shop                      = models.ForeignKey('Shop')
# 	bar_text 				  = models.TextField(default="")
# 	bar_color 				  = models.TextField(default="black")
# 	bar_enable 				  = models.BooleanField(default=False)
# 	spending_goal 			  = models.FloatField(default=0)
# 	below_sg_text 			  = models.TextField(default="")
# 	above_sg_text 			  = models.TextField(default="")
# 	timer_enable 			  = models.BooleanField(default=False)
# 	timer_countdown 		  = models.IntegerField(default=0)
# 	created_on                = models.DateTimeField(auto_now_add=True)
# 	updated_on                = models.DateTimeField(auto_now_add=True)

