# -*- coding: utf-8 -*-
# Generated by Django 1.10.4 on 2017-08-19 14:38
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True
    #change
    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Location',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('lng', models.CharField(max_length=255)),
                ('lat', models.CharField(max_length=255)),
            ],
        ),
    ]
