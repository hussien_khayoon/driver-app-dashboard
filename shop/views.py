from django.shortcuts import render, redirect
from django.core.urlresolvers import reverse
from django.template import RequestContext
from django.conf import settings as django_settings
import logging
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponseNotFound, HttpResponse
from shop.models import *
import uuid
import json
import datetime
import os
from django.core import serializers
import requests
from django.http import JsonResponse
from django.core.cache import cache
logger = logging.getLogger('driverApp')

@csrf_exempt
def dashboard(request):

	# if  == '':
	# 	logger.error("No shop session found here")
	# 	return HttpResponseNotFound("No shop session found")

	# if request.method == 'POST':
	# 	shop   			= Shop.objects.filter(domain = shop_url).first()
	# 	barSettings 	= BarSettings.objects.filter(shop = shop).first()

	# 	bar_text   			= request.POST.get('bar_text')
	# 	bar_color  			= request.POST.get('bar_color')
	# 	bar_enable 			= request.POST.get('bar_enable')
	# 	timer_countdown 	= request.POST.get('timer_countdown')
	# 	timer_enable 		= request.POST.get('timer_enable')

	# 	if not barSettings:
	# 		barSettings                  = BarSettings()
	# 		barSettings.shop             = shop

	# 	# Make sure we actually send a color
 #  		barSettings.bar_color 		= bar_color if bar_color else barSettings.bar_color
	# 	barSettings.bar_enable 		= True if bar_enable else False
	# 	barSettings.bar_text   		= bar_text
	# 	barSettings.timer_countdown = timer_countdown
	# 	barSettings.timer_enable    = True if timer_enable else False

	# 	barSettings.save()

	# 	cache.set(shop_url+'-barsettings', barSettings)

	# 	return JsonResponse({
	# 		'bar_text': 		bar_text,
	# 		'bar_color': 		bar_color,
	# 		'bar_enable': 		bar_enable,
	# 		'timer_countdown':  timer_countdown,
	# 		'timer_enable': 	timer_enable
	# 	})


		return render(request, 'shop/dashboard.phtml',
		{
			'production': django_settings.DEPLOYED
		})

@csrf_exempt
def coords(request):
	# test
	if request.method == 'POST':

		params 		= json.loads(request.body)

		location   	= Location.objects.filter(id=1).first()

		if location:
			lat 			= params['lat']
			lng 			= params['lng']
			location.lat 	= lat
			location.lng  	= lng
			location.save()
		else:
			location = Location()
			location.lat = '43.6532'
			location.lng = '-79.3832'
			location.save()



		data 	= {'success': 'your mom', 'lat': location.lat, 'lng': location.lng}

		response = JsonResponse(data)

		return response
	else:
		lat 		= ''
		lng 		= ''
		location   	= Location.objects.filter(id=1).first()

		if not location:
			lat = '43.6532'
			lng = '-79.3832'
		else:
			lat = location.lat
			lng = location.lng

		data 	= {'lat': lat, 'lng': lng}

		response = JsonResponse(data)

		return response
