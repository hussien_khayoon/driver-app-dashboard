from django.conf.urls import *
from django.contrib import admin
from django.conf import settings as django_settings
from django.conf.urls.static import static
admin.autodiscover()

from shop import views as shop_views

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^dashboard/?$', shop_views.dashboard, name='dashboard'),
    url(r'^coords/', shop_views.coords, name='coords'),
    # url(r'^webhooks/uninstall',    'shop.views.app_uninstall')
]
