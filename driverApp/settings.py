"""
Django settings for driverApp project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
from utility import getBooleanFromValue
BASE_DIR = os.path.dirname(os.path.dirname(__file__))

import logging
logger = logging.getLogger('driverApp')

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '$faix4@8$hw#j03cv418aivh=)w6cwfgtz$vq#xnj=sn-wq%--'

# SECURITY WARNING: don't run with debug turned on in production!

# DEPLOYED       = getBooleanFromValue(os.environ.get("DEPLOYED", "False"))
# USE_SSL        = getBooleanFromValue(os.environ.get('USE_SSL', 'True' if DEPLOYED  else 'False'))
# DEBUG          = getBooleanFromValue(os.environ.get('DEBUG',   'False' if DEPLOYED  else 'True'))
# TEMPLATE_DEBUG = getBooleanFromValue(os.environ.get('TEMPLATE_DEBUG',   'False' if DEPLOYED  else 'True'))

DEPLOYED       = getBooleanFromValue(os.environ.get("DEPLOYED", "False"))
USE_SSL        = getBooleanFromValue(os.environ.get('USE_SSL', 'True' if DEPLOYED  else 'False'))
DEBUG          = getBooleanFromValue(os.environ.get('DEBUG',   'False' if DEPLOYED  else 'True'))
TEMPLATE_DEBUG = getBooleanFromValue(os.environ.get('TEMPLATE_DEBUG',   'False' if DEPLOYED  else 'True'))
APP_URL        = 'https://driver-app-dashboard.herokuapp.com/' if DEPLOYED else 'http://127.0.0.1:8000/'

# TODO: THIS IS TEMPORARY DO NOT FORGET IN PRODUCTION
ALLOWED_HOSTS = ['*']


# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'storages',
    'shop',
    'sslserver',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'driverApp.urls'

WSGI_APPLICATION = 'driverApp.wsgi.application'

#USE_SSL = True


# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases

'''
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}
'''

DEPLOYED = os.environ.get("DEPLOYED", False)

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': os.environ.get('DB_NAME', 'driverApp'),
        'USER': os.environ.get('DB_USERNAME', 'root'),
        'PASSWORD': os.environ.get('DB_PASSWORD', ''),
        'HOST': os.environ.get('DB_DOMAIN', 'localhost'),   # Or an IP Address that your DB is hosted on
        'PORT': os.environ.get('DB_PORT', '3306')
    }
}

# Update database configuration with $DATABASE_URL.
import dj_database_url
db_from_env = dj_database_url.config(conn_max_age=500)
DATABASES['default'].update(db_from_env)


# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.10/howto/static-files/

AWS_STORAGE_BUCKET_NAME = os.environ.get("AWS_STORAGE_BUCKET_NAME", '')
AWS_ACCESS_KEY_ID = os.environ.get("AWS_ACCESS_KEY_ID", '')
AWS_SECRET_ACCESS_KEY = os.environ.get("AWS_SECRET_ACCESS_KEY", '')

# Tell django-storages that when coming up with the URL for an item in S3 storage, keep
# it simple - just use this domain plus the path. (If this isn't set, things get complicated).
# This controls how the `static` template tag from `staticfiles` gets expanded, if you're using it.
# We also use it in the next setting.

AWS_S3_CUSTOM_DOMAIN = '%s.amazonaws.com' % AWS_STORAGE_BUCKET_NAME

# This is used by the `static` template tag from `static`, if you're using that. Or if anything else
# refers directly to STATIC_URL. So it's safest to always set it.
#STATIC_URL = "https://%s/" % AWS_S3_CUSTOM_DOMAIN


# Tell the staticfiles app to use S3Boto storage when writing the collected static files (when
# you run `collectstatic`).

if DEPLOYED:
    STATIC_URL = "https://%s/" % AWS_S3_CUSTOM_DOMAIN
    STATICFILES_STORAGE = 'storages.backends.s3boto.S3BotoStorage'
else:
    STATIC_URL = '/static/'

STATIC_ROOT = os.path.join(BASE_DIR, 'staticfiles')
# Extra places for collectstatic to find static files.
STATICFILES_DIRS = (
    os.path.join(BASE_DIR, 'static'),
)

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                "django.contrib.auth.context_processors.auth",
            ]
        }
    }
]

SHOPIFY_APP_KEY = os.environ.get('SHOPIFY_APP_KEY', 'e2353d0176be256be46561f9316b7ac1')
SHOPIFY_SHARED_SECRET = os.environ.get('SHOPIFY_SHARED_SECRET', '9d592634f006ff7154261df433f37c2b')
SHOPIFY_API_SCOPE = ['read_products', 'read_customers', 'read_orders', 'write_script_tags', 'write_themes']



ADMINS = (
    ('Hussien', 'gorelami@gmail.com')
)

SERVER_EMAIL = os.environ.get('SERVER_EMAIL', 'django-local@toocoomedia.com')


LOG_ROOT = os.environ.get("LOG_ROOT", "logs")

# EMAIL_BACKEND = 'django.core.mail.backends.filebased.EmailBackend'
# EMAIL_FILE_PATH = '/tmp/app-messages' # change this to a proper location

LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'formatters': {
        'verbose': {
            'format': '%(levelname)s %(asctime)s %(thread)d %(funcName)s() line: %(lineno)d %(message)s'
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
    },
    'handlers': {
        'null': {
            'level': 'DEBUG',
            'class': 'logging.NullHandler',
        },
        'console':{
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'simple'
        },
        'file': {
            'level': 'DEBUG',
            'class':'logging.FileHandler',
            'filename': os.path.join(LOG_ROOT,'driverApp.log'),
            'formatter':'verbose',
        },
        'mail_admins': {
            'level': 'ERROR',
            'class': 'django.utils.log.AdminEmailHandler',
            'include_html': True,
            'email_backend': 'django.core.mail.backends.smtp.EmailBackend',
        }
    },
    'loggers': {
        'django': {
            'handlers': ['console'],
            'propagate': True,
            'level': 'INFO',
        },
        'django.request': {
            'handlers': ['file', 'console'],
            'level': 'ERROR',
            'propagate': False,
        },
        'driverApp': {
            'handlers': ['console', 'file'],
            'level': 'DEBUG',
        },
    }
}