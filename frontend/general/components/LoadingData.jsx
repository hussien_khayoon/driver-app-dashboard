import React, {Component, PropTypes}  	from 'react'


export default class LoadingData extends Component
{
	state = {
		loading: 	true
	}

	render()
	{
		const {color = '#41b374', dipslay, text} 	= this.props;

		const displayStyle 				= {display: display ? 'block' : 'none'};
		const backGroundColorStyle 		= {backgroundColor: color};

		return(
			<div id="loading-data" className="wrapper " style={displayStyle}>
			  <div className="row text-center" style={{ paddingTop:'50px', paddingBottom:'50px'}}>
				  <div className="col-xs-2"></div>
				  <div className="col-xs-8">
					  <div className="spiner-example">
						  <div className="sk-spinner sk-spinner-cube-grid">
							  <div className="sk-cube" style={backGroundColorStyle}></div>
							  <div className="sk-cube" style={backGroundColorStyle}></div>
							  <div className="sk-cube" style={backGroundColorStyle}></div>
							  <div className="sk-cube" style={backGroundColorStyle}></div>
							  <div className="sk-cube" style={backGroundColorStyle}></div>
							  <div className="sk-cube" style={backGroundColorStyle}></div>
							  <div className="sk-cube" style={backGroundColorStyle}></div>
							  <div className="sk-cube" style={backGroundColorStyle}></div>
							  <div className="sk-cube" style={backGroundColorStyle}></div>
						  </div>
						  <br/>
						  <h2 className="modal-title text-center">{text}</h2>
					  </div>
				  </div>
				  <div className="col-xs-2"></div>
			  </div>
		  </div>
		);
	}
}

LoadingData.propTypes = {
	loading: 	PropTypes.bool.isRequired,
	text: 		PropTypes.string.isRequired,

	display: 	PropTypes.string,
	color: 		PropTypes.string,
}