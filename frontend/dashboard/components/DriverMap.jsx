import React, {Component, PropTypes} 	from 'react'
import {Gmaps, Marker} 					from 'react-gmaps';

const params = {v: '3.exp', key: 'AIzaSyCVvuGr5bG7aP07PkhSt65VPb_KznRWozk'};


export default class DriverMap extends Component
{
	onMapCreated = (map) =>
	{
		map.setOptions({
			disableDefaultUI: true
		});
	}

	render()
	{
		const defaultCoords 						= {lat: 43.6532, lng: -79.3832};
		const {currentCoords = defaultCoords, coordinates} 		= this.props;
		console.log(coordinates)

		return(
			<div className="dirver-map">
				<Gmaps

					height={'500px'}
					lat={coordinates.lat}
					lng={coordinates.lng}
					zoom={16}
					loadingMessage={'Loading Map'}
					params={params}
					onMapCreated={this.onMapCreated}
				>
					<Marker
					  lat={coordinates.lat}
					  lng={coordinates.lng}
					  icon="https://emoji.slack-edge.com/T0KEW4CSV/steven/666b0c6d79db1478.png"
					/>
				</Gmaps>
      		</div>
		);
	}
}

DriverMap.propTypes = {
}