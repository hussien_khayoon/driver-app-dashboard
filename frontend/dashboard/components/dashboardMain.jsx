import React, { Component }               from 'react';

import DriverMap 						  from './DriverMap.jsx'

export default class DashboardMain extends Component
{
  	render()
  	{
    	return (
      		<div className="landing-main">
            	<DriverMap {...this.props} />
      		</div>
   		);
  	}
}

DashboardMain.propTypes = {

}
