const initialState = {
    isLandingSignUpDone: false
}

export default function signUpReducer(state = initialState, action)
{
    switch (action.type)
    {
        case 'LANDING_SIGNUP_DONE':
            return {
                ...state,
                isLandingSignUpDone:          action.isLandingSignUpDone
            }

        default:
            return state
    }
}