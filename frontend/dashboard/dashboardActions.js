import Toastr 											from 'toastr'

import * as types                                       from './dashboardActionTypes'
import * as DataAPI 									from '../api/DataAPI'

/*
** Action creator
*/
export function landingSignUpDone(isLandingSignUpDone)
{
	return{
		type: 		types.LANDING_SIGNUP_DONE,
		isLandingSignUpDone
	}
}

/*
** Async Thunk for API calls
*/
export function landingSignUp(formData)
{
	return async (dispatch, getState) =>
	{
		dispatch({type: 	types.LANDING_SIGNUP_FETCH});

		try
		{
			const response = await DataAPI.landingSignUp(formData);

			dispatch({type: 	types.LANDING_SIGNUP_SUCCESS});
			dispatch(landingSignUpDone(true));
		}
		catch(error)
		{
			dispatch({type: 	types.LANDING_SIGNUP_FAILURE});

			const errorMessage = error.response.data ? error.response.data.errorMessage : 'Please Try Again';
			console.error("An Error occured with validateUserEmail", errorMessage);
			Toastr.error(errorMessage);
		}
	}
}