import React, { Component, PropTypes } 	from 'react'
import {connect} 						from 'react-redux'
import { bindActionCreators }   		from 'redux'

import * as DashboardActions 			from '../dashboardActions'
import DashboardMain 					from '../components/DashboardMain.jsx'

import '../styles/dashboard.scss'


class DashboardContainer extends Component
{
	state = {
		coordinates: {lat: '', lng: ''}
	}

	componentDidMount()
	{
        setInterval(() =>
        {
			$.ajax(
	        {
	            url: `/coords/`,
	            dataType: "json",
	            type: "GET",
	            success: (response) =>
	            {
	            	console.log('called')

	            	this.setState({
	            		coordinates: {
	            			lat: response.lat,
	            			lng: response.lng
	            		}
	            	})
	            },
	            error: (xhr, status, err) =>
	            {
	                alert('something went wrong');
	            }
	        });
        },1000)
	}
	render()
	{
		return(
			<DashboardMain
				coordinates={this.state.coordinates}
				{...this.props}
			/>
		);
	}
}

DashboardContainer.propTypes = {

}

const mapStateToProps = (state, props) =>
{
	return {
		...state.dashboardReducer
	}
}

const mapDispatchToProps = dispatch =>
{
	return {
		dashboardActions: 	bindActionCreators(DashboardActions, dispatch)
	}
}


export default connect(mapStateToProps, mapDispatchToProps)(DashboardContainer);



