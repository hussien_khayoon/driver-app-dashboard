import React 									from 'react'
import { Route, IndexRoute, Redirect } 			from 'react-router'


// Main Routes
import dashboardContainer 			from './dashboard/containers/dashboardContainer.jsx'


const createRoutes = (store) => {
	return(
		<Route>
			<Route path="/dashboard"    	component={dashboardContainer} />
		</Route>
	);
}


export default createRoutes;
