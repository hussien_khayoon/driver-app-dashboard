import moment 		from 'moment'


export default function capitalize(string)
{
	return string.charAt(0).toUpperCase() + string.slice(1);
}

// Sleep for ms
export function sleep(ms)
{
  return new Promise(resolve => setTimeout(resolve, ms));
}

// Format Date using momentJS
export function formatDate(inputDateString, format)
{
    if(inputDateString)
    {
        return(moment(inputDateString, 'YYYY-MM-DD').format(format));
    }
    else
    {
        return '';
    }
}

// Validation for Email using Regex
export function emailValidation(emailValue)
{
	let isEmailValid = true;
    if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(emailValue))
    {
    	isEmailValid = false;
    }

    return isEmailValid;
}

// Validation for Phone Number using Regex
export function phoneNumValidation(phoneNumValue)
{
	let isPhoneNumValid = true;
    if (!/\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/.test(phoneNumValue))
    {
    	isPhoneNumValid = false;
    }

    return isPhoneNumValid;
}


/*
** a Custom groupBy function, that maintain the original order of the array
*/
export function groupByKeepOrder(arr, prop)
{
    let newArr = [] // array to return, keeps track of order
      , wrapObj = {}; // temporary object used for grouping

    arr.forEach( item =>
    {
        if(item)
        {
            // gets property name to group by and converts it to a string for grouping purposes
            let propName = item[prop];
            if(propName)
            {
                propName = propName.toString();

                // checks if group exists already and creates a new group if not,
                // pushing it into the array to return ('newArr')
                if (!wrapObj[propName])
                {
                    wrapObj[propName] = [];
                    newArr.push(wrapObj[propName]);
                }

              // adds item to the group
              wrapObj[propName].push(item);
            }
            else
            {
                console.warn("utils -> groupByKeepOrder", "Property '" + prop + "' not found within object. It will be ignored from the output.", item);
            }
        }
    });

    return newArr
}