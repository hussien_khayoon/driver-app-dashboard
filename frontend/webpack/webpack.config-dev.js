const webpack 			= require('webpack');
const path    			= require('path');

const BUILD_DIR = path.resolve(__dirname, '../../staticfiles/js/dist/build');
const APP_DIR   = path.resolve(__dirname, '../app/containers');

const config =
{
	context: APP_DIR,
	entry:{
		app:[
			'react-hot-loader/patch',
			'webpack-dev-server/client?http://localhost:3000', // WebpackDevServer host and port
			'webpack/hot/only-dev-server', // "only" prevents reload on syntax errors
			'babel-polyfill',
			'./app/app.js'
		]
	},
	output:
	{
		path: BUILD_DIR,
		filename: '[name].js',
		publicPath: 'http://localhost:3000/static/js/dist/build/' // this line is imperative
	},
	devtool: 'eval',
	plugins:onCreatePlugins(),
	module :
	{
		rules : [
			{
				test: /\.jsx?$/,
				loader: 'babel-loader',
				exclude: /node_modules/
		    },
			{
				test: /\.css?$/,
		        use:[
		      		'style-loader',
		      		'css-loader'
		      	]
			},
			// SASS
			{
				test: /\.scss$/,
				use:[
					'style-loader',
					'css-loader',
					'sass-loader'
				],
				exclude: /node_modules/
			},
			{
				test: /\.(png|jpg)$/,
				use:[
		      	{
		      		loader: 'file-loader'
		      	}
		      ]
			}
		]
	}
};


//
// Helpers
//
function onCreatePlugins()
{
	var plugins = [];

	plugins.push(
		new webpack.optimize.CommonsChunkPlugin({
			name: 		"init",
			minChunks: function (module)
			{
               // this assumes your vendor imports exist in the node_modules directory
               return module.context && module.context.indexOf('node_modules') !== -1;
            }
		})
	);

	plugins.push(
		new webpack.ProvidePlugin({
			_: "underscore"
		})
	);

	plugins.push(

		new webpack.DefinePlugin(
		{
			__DEV__: false,
			__PROD__: false,
			__TEST__: true
		}),

		new webpack.DefinePlugin({
			'process.env': {
			  'NODE_ENV': JSON.stringify('developement')
			}
		}),

		new webpack.HotModuleReplacementPlugin(),

		new webpack.NoEmitOnErrorsPlugin()
	);

	return plugins;
}

module.exports = config;