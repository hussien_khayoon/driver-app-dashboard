const webpack = require('webpack');
const path    = require('path');

const BUILD_DIR = path.resolve(__dirname, '../../staticfiles/js/dist/build');
const APP_DIR   = path.resolve(__dirname, '../app/containers');

const config =
{
	context: APP_DIR,
	entry:
	{
		app: ['babel-polyfill', './app/app.js'],
	},
	output:
	{
		path: BUILD_DIR,
		filename: '[name].js',
		sourceMapFilename: '[file].map'
	},
	devtool: 'source-map',
	plugins: onCreatePlugins(),
	module :
	{
		rules : [
			{
				test: /\.jsx?$/,
				loader: 'babel-loader',
				exclude: /node_modules/
		    },
			{
				test: /\.css?$/,
				use:[
		      		'style-loader',
		      		'css-loader'
		      	]
			},
			// SASS
			{
				test: /\.scss$/,
				use:[
					'style-loader',
					'css-loader',
					'sass-loader'
				],
				exclude: /node_modules/
			},
			{
				test: /\.(png|jpg)$/,
				use:[
		      	{
		      		loader: 'file-loader'
		      	}
		      ]
			}
		]
	}
};


//
// Helpers
//
function onCreatePlugins()
{
	var plugins = [];

	plugins.push(
		new webpack.optimize.CommonsChunkPlugin({
			name: 		"init",
			minChunks: function (module)
			{
               // this assumes your vendor imports exist in the node_modules directory
               return module.context && module.context.indexOf('node_modules') !== -1;
            }
		})
	);

	plugins.push(
		new webpack.ProvidePlugin({
			_: "underscore"
		})
	);

	// Only Production Plugins
	plugins.push(

		new webpack.optimize.UglifyJsPlugin(
		{
			mangle: false,
			sourceMap: true
		}),

		new webpack.DefinePlugin(
		{
			__DEV__:  false,
			__PROD__: true,
			__TEST__: false
		}),

		new webpack.DefinePlugin({
			'process.env': {
			  'NODE_ENV': JSON.stringify('production')
			}
		})

	);

	return plugins;
}

module.exports = config;