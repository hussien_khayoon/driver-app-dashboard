import { createStore, applyMiddleware } from 'redux'
import thunkMiddleware 					from 'redux-thunk'
import { routerMiddleware } 			from 'react-router-redux'
import { browserHistory }               from 'react-router'
import Raven                            from "raven-js"
import createRavenMiddleware            from "raven-for-redux"

import AppReducer                       from '../app/appReducer.js'


export default function configureStore(preloadedState)
{
	const middleware    = routerMiddleware(browserHistory);

    const store = createStore(
        AppReducer,
        preloadedState,
        applyMiddleware(middleware, thunkMiddleware,
            // We init Raven here, so we can use RavenMiddleware to get:
            // 1) Redux action breadcrubms (useful for debugging)
            // 2) current state of the store (not working here, so we use the dataCallback to insert our own)
            createRavenMiddleware(Raven,
            {
                // Don't return entire state for now since it can be too large and cause
                // the sentry logger api call to fail.
                stateTransformer: (state) => { return {} },
            })
        )
    )

    return store
}