import axios 		from 'axios'
import querystring  from 'querystring'


/*
** Landing Page Calls
*/
export function landingSignUp(data)
{
	const endpoint = `/api/v1/landing/signup/`;

	return axios({
		method 	: 'POST',
		url 	: endpoint,
		data 	: querystring.stringify(data)
	});
}


/*
** SignUp Calls
*/
export function fetchPublicVendors()
{
	const endpoint = `/api/v1/vendors/public/`;

	return axios.get(endpoint);
}


// Validate username at SignUp
export function validateUserEmail(email)
{
	const endpoint = `/api/v1/account/validate/${email}/`;

	return axios({
		method: 'post',
		url: 	endpoint
	});
}

// Sign up a new user
export function signUpUser(data)
{
	const endpoint = '/api/v1/signup/';

	return axios({
		method: 'post',
		url: 	endpoint,
		data
	});
}