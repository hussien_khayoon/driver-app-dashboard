import React, { Component, PropTypes } 		from 'react'
import { Provider } 						from 'react-redux'
import { Router, browserHistory } 			from 'react-router'
import { syncHistoryWithStore } 			from 'react-router-redux'

import createRoutes 						from '../../../routes'
import DevTools 						    from '../DevTools'


export default class Root extends Component
{
	render()
	{
		const { store } = this.props;
		const history 	= syncHistoryWithStore(browserHistory, store);

		return (
			<Provider store={store}>
				<div>
					<Router history={history} routes={createRoutes(store)}>
					</Router>
				</div>
		  	</Provider>
		)
	}
}

Root.propTypes = {
	store: PropTypes.object.isRequired
}