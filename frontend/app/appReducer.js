import { combineReducers } 			from 'redux'
import { routerReducer } 			from 'react-router-redux'
import { reducer as formReducer } 	from 'redux-form'


import dashboardReducer 			from '../dashboard/dashboardReducer'


const AppReducer = combineReducers({
	dashboardReducer,
	routing: 	routerReducer,
	form: 		formReducer
})

export default AppReducer;