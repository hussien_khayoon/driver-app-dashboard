import ReactDOM 					from 'react-dom'
import React 						from 'react'
import Raven                        from 'raven-js'
import toastr                       from 'toastr'

import configureStore               from '../../../store/configureStore'
import Root                         from '../../components/Root/Root'

import "../../styles/app.scss"


const store     = configureStore();


ReactDOM.render(
 	<Root store={store} />,
	document.getElementById('root')
)

toastr.options = {
    "closeButton": true,
    "debug": false,
    "progressBar": false,
    "preventDuplicates": true,
    "positionClass": "toast-top-right",
    "onclick": null,
    "showDuration": "400",
    "hideDuration": "1000",
    "timeOut": "1500",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
}


