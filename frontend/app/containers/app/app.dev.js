import ReactDOM 					from 'react-dom'
import React 						from 'react'
import { AppContainer } 			from 'react-hot-loader'
import toastr                       from 'toastr'


import configureStore 				from '../../../store/configureStore'
import Root 						from '../../components/Root/Root'

import "../../styles/app.scss"


const store     = configureStore();

ReactDOM.render(
 	<AppContainer>
 		<Root store={store} />
	</AppContainer>,
	document.getElementById('root')
);

if (module.hot)
{
	module.hot.accept('../../components/Root/Root', () =>
	{
		const NextRoot = require('../../components/Root/Root').default;

		ReactDOM.render(
 			<AppContainer>
		 		<NextRoot store={store} />
			</AppContainer>,
			document.getElementById('root')
		);
	});
}

if (!window.google)
{
    const script    = document.createElement('script');
    script.src      = '//maps.googleapis.com/maps/api/js?key=AIzaSyC4ELGDS83CB9Lt-HWDSZkGThUoc0rw9j0&libraries=places';
    script.async    = 1;
    document.body.appendChild(script);
}


toastr.options = {
    "closeButton": true,
    "debug": false,
    "progressBar": false,
    "preventDuplicates": true,
    "positionClass": "toast-top-right",
    "onclick": null,
    "showDuration": "400",
    "hideDuration": "1000",
    "timeOut": "1500",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
}