Deploy Process on Heroku:

1) Push code to github
git add/commit/push

2) Push last commit to Heroku
git push heroku master

3) Upload static assets to S3 after deploying code to Heroku
 heroku run python manage.py collectstatic --noinput


 # Mac problems

1) pip install mysql-python fails with EnvironmentError: mysql_config not found
solution: export PATH=$PATH:/usr/local/mysql/bin

2) pg_config executable not found
solution: brew install postgresql

3) first time running `python manage.py runserver'
ValueError: Unable to configure handler 'file': [Errno 2] No such file or directory: '/Users/hussienkhayoon/apps/vendorhero-driver-backend/logs/driverApp.log'
solution: create /logs/app.log folder and file

4) Can't connect to local MySQL server through socket
solution: you need start mysql
mysql.server start

5) database not found
solution: create a new database manually

6) python-2.7.13 on runtime.txt to make heroku run python 2.7
